#include<cstdio>
#include<list>

using namespace std;

int main(void)
{
	int t;
	freopen("input", "r", stdin);
	scanf("%d", &t);
	for (int count = 0; count < t; count++)
	{
		char ch;
		int k;
		list<char> digits;
		digits.push_back('9');
		scanf("%c", &ch);
		while(ch == '\n')
			scanf("%c", &ch);
		while (ch != ' ')
		{
			digits.push_back(ch);
			scanf("%c", &ch);
			while(ch == '\n')
				scanf("%c", &ch);
		}
		scanf("%d", &k);
		// test printf("k:%d\n", k);
		list<char>::iterator cur = digits.begin();
		list<char>::iterator prev = cur;
		cur++;
		while (cur != digits.end())
		{
			// test printf("dig: %c\n", *cur);
			while (*prev < *cur)
			{
				prev = digits.erase(prev);
				prev--;
				// test printf("dig: %c\n", *prev);
				k--;
				// test printf("k: %d\n", k);
				if (k == 0)
					break;
			}
			if (k == 0)
				break;
			prev = cur;
			cur++;
		}
		while (k-- > 0) 
		{
			digits.pop_back();
		}
		cur = digits.begin();
		for (++cur; cur != digits.end(); cur++)
		{
			printf("%c", *cur);
		}
		printf("\n");
	}
	return 0;
}
