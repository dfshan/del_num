=========================================
算法与算法复杂性理论
=========================================
---------------------
第二次实验 解题报告
---------------------

.. sectnum::

实验题目
========

问题描述
----------

　　现有一个n位数，你需要删除其中的k位，请问如何删除才能使得剩下的数最大？

　　比如当数为2319274 k=1时，删去2变成319274后是可能的最大值。	

输入格式
---------

　　输入的第一行包含一个整数T，表示数据的组数。

　　接下来T组数据中：每组输入的仅有一行包含1个n位数以及数k，内容如前所述。

输出格式
---------

	对于每组测试数据，输出将n删除k位数后的最大数，数据保证最大数>0。

样例输入
---------
	2

	8800932422 1
	
	4736167090 2

样例输出
---------

	880932422

	76167090

数据规模和约定
---------------
　　对于80%的数据，满足 0<n<=2000, k<=min(1000,n-1)。

　　所有的数据满足 T=40 0<n<=100000, k<=min(1000,n-1)。

解题思路
========

	假设:

	* num[k] 为一个数删除 k 位数后的最大数

	* del_num(num, k) 表示把数 num 删除 k 位后的最大数的程序

	* 令 num[0] 表示原数，那么 num[k] = del_num(num[0], k)。

	那么，显然有如下结论::

		del_num(num[0], k) = del_num(num[k-1], 1)
	
	因此，原问题可以通过迭代 k 次对一个数删除一位使之最大的例程来实现:

	.. code:: cpp

		for i = 1 to k
			num[k] =  del_num(num[k], 1)

	下面考虑问题::
	
		对一个 n 位数 num，删除 1 位使之最大
	
	对该问题，假设数 num 的第 k 位（从左往右数）为 dig[k]， 那么有如下结论::

		1. 如果存在 i < n, 使得 d[i] < d[i+1]，那么删除第 j 位数字会得到的数是最大的，其中， j = min{j: d[j] < d[j+1]}
		2. 如果不存在 i < n, 使得 d[i] < d[i+1]，即 num 的 n 位数是单调递减的，那么删除最后一位数字得到的数是最大的

	结论 1 的证明如下::

		反证法：假设删除的数字 i 使得到的数最大，i != j，那么，分两种情况考虑：
		1. i < j：此时，删除第 i 位数字后处于第 i 位的数字将是 d[i+1]。而删除第 j 位数字后处于第 i 位的数字是 d[i]，由于 d[i] > d[i+1]，且与删除第 j 位的结果相比，两者结果的前 i-1 位数字相同,因此删除第 j 位数字可以产生更大的数（前 i-1 位数字相同），与得到的数最大发生矛盾。
		2. i > j: 此时，删除第 i 位数字后，处于第 j 位的数字是 d[j]，而删除第 j 位数字后，处于第 j 位的数字是 d[j+1]，由于两种删除方法的前 j-1 位数字都相同，因此删除第 j 位数字可以产生更大的数（前 i-1 位数字相同），与得到的数最大发生矛盾。

	结论 2 的证明如下::
	
		反证法：假设删除第 i (< n) 位数字使得得到的数最大，那么与删除第 n 位数字的结果相比，前 i-1 位数字相同，第 i 位数字分别为 d[i-1] 和 d[i]，由于 d[i] > d[i-1]，因此显然删除第 n 位数字得到的数更大，与条件矛盾
	
	因此，可以将前面给出的算法改造成一个迭代的算法。
	从左到右扫描数字，如果数字的值增加，则删除前面的数。
	程序结束前，如果所有的数字的值从左到右依次减小，那么删除尾部的数字。
	算法的流程图如下所示。

	由于该算法的至多对每个数字扫描 2 次，故算法的 **时间复杂度** 为 O(n).

.. image:: fc.svg
    :target: https://www.dropbox.com/s/7lvsvwg602olx0y/fc.svg
	:align: center
	:width: 500px



实验结果
========
	能在规定的时间限制内解决问题，如下图所示：
	
.. image:: result.png
	:target: https://www.dropbox.com/s/iqii352gksejcqv/result.png
	:align: center

程序源码
========

.. code:: cpp

  #include<cstdio>
  #include<list>
  using namespace std;
  int main(void)
  {
	  int t;
	  freopen("input", "r", stdin);
	  scanf("%d", &t);
	  for (int count = 0; count < t; count++)
	  {
		  char ch;
		  int k;
		  list<char> digits;
		  digits.push_back('9');
		  scanf("%c", &ch);
		  while(ch == '\n')
			  scanf("%c", &ch);
		  while (ch != ' ')
		  {
			  digits.push_back(ch);
			  scanf("%c", &ch);
			  while(ch == '\n')
				  scanf("%c", &ch);
		  }
		  scanf("%d", &k);
		  // test printf("k:%d\n", k);
		  list<char>::iterator cur = digits.begin();
		  list<char>::iterator prev = cur;
		  cur++;
		  while (cur != digits.end())
		  {
			  // test printf("dig: %c\n", *cur);
			  while (*prev < *cur)
			  {
				  prev = digits.erase(prev);
				  prev--;
				  // test printf("dig: %c\n", *prev);
				  k--;
				  // test printf("k: %d\n", k);
				  if (k == 0)
					  break;
			  }
			  if (k == 0)
				  break;
			  prev = cur;
			  cur++;
		  }
		  while (k-- > 0) 
		  {
			  digits.pop_back();
		  }
		  cur = digits.begin();
		  for (++cur; cur != digits.end(); cur++)
		  {
			  printf("%c", *cur);
		  }
		  printf("\n");
	  }
	  return 0;
  }

